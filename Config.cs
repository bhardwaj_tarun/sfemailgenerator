﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SFEmailGenerator
{
    public class Config
    {
        private static string _to_be_saved_emls_loc;
        private static string _fromEmailAddress;
        private static string _toEmailAddresses;
        private static string _FolderFilter;
        private static bool _FolderIncludeSubDirectories;
        private static double _timerInterval;
        private static int _filesInInterval;
        private static bool _isEmlEditable;

        static Config()
        {
            _to_be_saved_emls_loc = ConfigurationManager.AppSettings["EMLsLoc"];
            _isEmlEditable = Convert.ToBoolean(ConfigurationManager.AppSettings["IsEmlEditable"]);
            _fromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"];
            _toEmailAddresses = ConfigurationManager.AppSettings["ToEmailAddresses"];
            _FolderFilter = ConfigurationManager.AppSettings["FolderFilter"];
            _FolderIncludeSubDirectories = Convert.ToBoolean(ConfigurationManager.AppSettings["FolderIncludeSubDirectories"]);
            _timerInterval = Convert.ToDouble(ConfigurationManager.AppSettings["TimerInterval"]);
            _filesInInterval = Convert.ToInt32(ConfigurationManager.AppSettings["FilesInInterval"]);
        }

        public static bool IsEmlEditable
        {
            get
            {
                return _isEmlEditable;
            }
        }
        public static string FromEmailAddress
        {
            get
            {
                return _fromEmailAddress;
            }
        }

        public static string ToEmailAddresses
        {
            get
            {
                return _toEmailAddresses;
            }
        }

        public static int FilesInInterval
        {
            get
            {
                return _filesInInterval;
            }
        }

        public static double TimerInterval
        {
            get
            {
                return _timerInterval;
            }
        }

        public static string EmailsSavedtoLocation
        {
            get
            {
                return _to_be_saved_emls_loc;
            }
        }
        public static string FolderFilter
        {
            get
            {
                return _FolderFilter;
            }
        }
        public static bool FolderIncludeSubDirectories
        {
            get
            {
                return _FolderIncludeSubDirectories;
            }
        }
    }
}
