﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Configuration.Install;
using System.Reflection;

namespace SFEmailGenerator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            try
            {
                //#if DEBUG
                //While debugging this section is used.
                //EmailGeneratorService myService = new EmailGeneratorService();
                //myService.onDebug();
                //System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
                //#else
                //In Release this section is used. This is the "normal" way

                if (Debugger.IsAttached)    /// this is used when debug
                {
                    EmailGeneratorService objService = new EmailGeneratorService();
                    objService.ManuallyRunService();
                }
                else if (Environment.UserInteractive)       /// this is used to get user action, like to listen install
                {
                    var parameter = string.Concat(args);
                    switch (parameter)
                    {
                        case "--install":
                            ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
                            break;
                        case "--uninstall":
                            ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
                            break;
                    }
                }
                else
                {
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[]
                    {
                        new EmailGeneratorService()
                    };
                    ServiceBase.Run(ServicesToRun);
                }
                //#endif
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("SFEmailGenerator", "Error - " + ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
