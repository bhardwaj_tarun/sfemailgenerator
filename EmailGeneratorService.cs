﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using System.Threading;
using log4net;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace SFEmailGenerator
{
    public partial class EmailGeneratorService : ServiceBase
    {
        private System.Timers.Timer TimerInterval;
        public static readonly ILog fileLogger = LogManager.GetLogger(typeof(EmailGeneratorService));
        EventLog eventLog;
        public EmailGeneratorService()
        {
            InitializeComponent();

            //Initialize timer
            this.TimerInterval = new System.Timers.Timer();
            this.TimerInterval.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
            this.TimerInterval.Interval = Config.TimerInterval;   /// initial timer

            ///Initialize application event logger
            eventLog = new EventLog();
            if (!EventLog.SourceExists("SFEmail_Generator"))
            {
                EventLog.CreateEventSource("SFEmail_Generator", "");
            }
            eventLog.Source = "SFEmail_Generator";
            //eventLog.Log = "SFEmail_Generator";   // if setting log then need to register source in it first

            string message = String.Format("Timer_Elapsed() => Starting to generating files with extension ({0}) in the folder ({1}) in every ({2}) milliseconds.", Config.FolderFilter, Config.EmailsSavedtoLocation, Config.TimerInterval);
            eventLog.WriteEntry(message, EventLogEntryType.Information);
            fileLogger.Info(message);
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                this.TimerInterval.Enabled = false;

                /// write your logic to Generate
                ///#1- Need to apply thread here so pointer get released for next timer cycle, free the timer
                ///Why? use "Async" approach here: This is typically used to give better response time for users by pushing long - running tasks into the background so the machine can be more responsive.
                Thread thread = new Thread(() => GenrateFiles());
                thread.Start();
            }
            catch (Exception Ex)
            {
                string message = "Timer_Elapsed() => File Creation Process Error: " + Ex.Message.ToString();
                eventLog.WriteEntry(message, EventLogEntryType.Error);
                fileLogger.Error(message);
            }
            finally
            {
                this.TimerInterval.Interval = Config.TimerInterval;   // for every TimerInterval
                this.TimerInterval.Enabled = true;
            }
        }

        public void onDebug()
        {
            /// in case service is running in developmnet/test mode locally
            OnStart(null);
        }

        //Method for testing functionality in debug mode
        public void ManuallyRunService()
        {
            string message = "ManuallyRunService() => File creation Manually Ran.";
            eventLog.WriteEntry(message, EventLogEntryType.Information);
            fileLogger.Info(message);

            GenrateFiles();
            /// or enable in Threaded manner
            //Thread thread = new Thread(() => GenrateFiles());
            //thread.Start();
        }

        private void RemoveUnencryptFile(string fileToDelete)
        {
            try
            {
                if (File.Exists(fileToDelete))
                {
                    File.Delete(fileToDelete);
                    fileLogger.Info(String.Format("RemoveUnencryptFile() => Unencrypted file ({0}) removed from the folder.", fileToDelete));
                }
            }
            catch (Exception ex)
            {
                string message = "RemoveUnencryptFile() => Exception caught: " + ex.Message.ToString();
                eventLog.WriteEntry(message, EventLogEntryType.Error);
                fileLogger.Error(message);
            }
        }

        protected override void OnStart(string[] args)
        {
            this.TimerInterval.Enabled = true;
        }

        protected override void OnStop()
        {
            this.TimerInterval.Enabled = false;

            string message = "OnStop() => Stopping to generating files.";
            eventLog.WriteEntry(message, EventLogEntryType.Information);
            fileLogger.Info(message);
        }

        public string UniqueID()
        {
            DateTime date = DateTime.Now;
            string uniqueID = String.Format("{0:0000}{1:00}{2:00}{3:00}{4:00}{5:00}{6:000}", date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Millisecond);

            return uniqueID;
        }
        public void GenrateFiles()
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(Config.EmailsSavedtoLocation);
                // Checks whether the directory is a valid location
                if (dir.Exists)
                {
                    Parallel.For(0, Config.FilesInInterval,
                    index =>
                    {
                        GenrateFile(index);
                    });

                    string message = String.Format("GenrateFiles() => {0} files created successfully.", Config.FilesInInterval);
                    fileLogger.Info(message);
                }
                else
                {
                    throw new Exception(string.Format("GenrateFiles() => Directory {0} is not exists.", Config.EmailsSavedtoLocation));
                }
            }
            catch (Exception ex)
            {
                string message = "GenerateFiles() => File Creation Process Error: " + ex.Message.ToString();
                eventLog.WriteEntry(message, EventLogEntryType.Error);
                fileLogger.Error(message);
            }
        }

        public void GenrateFile(int index)
        {
            try
            {
                string subject = string.Format("New email received at {0}, and index {1}.", UniqueID(), index);
                string body = message();

                using (var client = new SmtpClient())
                {
                    client.UseDefaultCredentials = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    client.PickupDirectoryLocation = Config.EmailsSavedtoLocation;

                    using (var message = new MailMessage())
                    {
                        message.From = new MailAddress(Config.FromEmailAddress, Config.FromEmailAddress);

                        string[] multiEmail = ExtractEmailAddressesFromString(Config.ToEmailAddresses);
                        foreach (string toEmail in multiEmail)
                        {
                            message.To.Add(new MailAddress(toEmail));
                        }

                        message.Subject = subject;
                        message.Body = body;

                        var htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
                        message.AlternateViews.Add(htmlView);

                        message.IsBodyHtml = true;

                        if (Config.IsEmlEditable)
                            message.Headers.Add("X-Unsent", "1");

                        message.BodyEncoding = Encoding.Unicode;
                        message.SubjectEncoding = Encoding.Unicode;

                        try
                        {
                            client.Send(message);
                        }
                        catch (Exception ex)
                        {
                            string msg = "GenrateFile() => File Creation Process Error: " + ex.Message.ToString();
                            eventLog.WriteEntry(msg, EventLogEntryType.Error);
                            fileLogger.Error(msg);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string message = "GenrateFile() => File Creation Process Error: " + ex.Message.ToString();
                eventLog.WriteEntry(message, EventLogEntryType.Error);
                fileLogger.Error(message);
            }
        }

        private string[] ExtractEmailAddressesFromString(string source)
        {
            try
            {
                MatchCollection mc;
                int i;

                mc = Regex.Matches(source, @"([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})");
                string[] results = new string[mc.Count];
                for (i = 0; i <= results.Length - 1; i++)
                    results[i] = mc[i].Value;

                return results;
            }
            catch (Exception ex)
            {
                string msg = string.Format("ExtractEmailAddressesFromString({0}) => Exception caught : {1}", source, ex.Message);
                eventLog.WriteEntry(msg, EventLogEntryType.Error);
                fileLogger.Error(msg);

                return new string[0];
            }
        }

        public string message()
        {
            return @"<style>
            body {
                font-family: Arial;
                font-size: 14pt;
            }

            table {
                border-spacing:5px;
            }

            table, th, td {
            border:1px solid black;
            border-collapse:collapse;
            }

            th, td {
            padding: 15px;
            }

            th {
                text-align:left;
            color: white;
                background-color:#4A6CE7;
                                    }

            table tr:nth-child(even) {
                background-color: #FFFFFF;
                                    }

            table tr:nth-child(odd) {
                background-color: #C0C0C0;
                                    }
                                </style>
<h1> ----------------This is a Test Mail------------- </h1>
<h2> Pre - Registration </h2>
<h3> New Form Submission</h3>
   <h3> Location: St.Joseph's Hospital</h3><table>
     <tr><th colspan = '2'> Patient Information </th></tr><tr><td> Patient First Name</td><td> test </td></tr>
                      <tr><td> Patient Middle Initial</td><td></td></tr>
                           <tr><td> Patient Last Name</td><td> test </td></tr>
                                <tr><td> Patient Maiden Name</td><td></td></tr>
                                     <tr><td> Outside of U.S.</td><td> No </td></tr>
                                          <tr><td> State of Birth</td><td> Alabama </td></tr>
                                               <tr><td> Country of Birth</td><td> United States </td></tr>
                                                         <tr><td> Patient DOB </td><td> 08 / 02 / 2020 </td></tr>
                                                              <tr><td> Patient SSN </td><td> 999 - 99 - 9999 </td></tr>
                                                                   <tr><td> Ethnicity </td><td> Non - Hispanic </td></tr>
                                                                   <tr><td> Preferred Language </td><td> English </td></tr>
                                                                        <tr><td> Religious Preference </td><td> Hindu </td></tr>
                                                                             <tr><td> Gender </td><td> Male </td></tr>
                                                                             <tr><td> Marital Status </td><td> Married </td></tr>
                                                                                  <tr><td> Race </td><td> Asian </td></tr>
                                                                                  <tr><td> Patient Address </td><td> test </td></tr>
                                                                                       <tr><td> City </td><td> test </td></tr>
                                                                                       <tr><td> Outside of U.S.</td><td> No </td></tr>
                                                                                            <tr><td> State </td><td> Alabama </td></tr>
                                                                                            <tr><td> Country </td><td> United States </td></tr>
                                                                                                       <tr><td> Zip Code </td><td> 85001 </td></tr>
                                                                                                            <tr><td> Telephone Number </td><td> 999 - 999 - 9999 </td></tr>
                                                                                                                 <tr><td> Cell Phone Number</td><td></td></tr>
                                                                                                                      <tr><td> Email Address </td><td> test@test.com </td></tr>
                                                                                                                                 <tr><td> Not Applicable </td><td> No </td></tr>
                                                                                                                                      <tr><td> Would you like to receive our eNewsletter?</td><td> No </td></tr>
                                                                                                                                           <tr><td> Phone </td><td> No </td></tr>
                                                                                                                                           <tr><td> Email </td><td> No </td></tr>
                                                                                                                                           <tr><td> Morning </td><td> No </td></tr>
                                                                                                                                           <tr><td> Afternoon </td><td> Yes </td></tr>
                                                                                                                                           <tr><td> Evening </td><td> No </td></tr>
                                                                                                                                           <tr><th colspan = '2' > Employment </ th ></tr><tr><td> Employment Status </td><td> Active Military Duty</td></tr>
                                                                                                                                                                  <tr><td> Employer Name </td><td> test </td></tr>
                                                                                                                                                                       <tr><td> Employer City </td><td> test </td></tr>
                                                                                                                                                                            <tr><td> Employer State </td><td> Alabama </td></tr>
                                                                                                                                                                                 <tr><td> Employer Zip </td><td> 85001 </td></tr>
                                                                                                                                                                                      <tr><td> Employer Phone </td><td> 999 - 999 - 9999 </td></tr>
                                                                                                                                                                                           <tr>< th colspan = '2' > Admission </ th ></tr><tr><td> Are you a returning patient?</td><td> No </td></tr>
                                                                                                                                                                                                            <tr><td> Date of last menstrual cycle</td><td></td></tr>
                                                                                                                                                                                                                 <tr><td> Are you pregnant?</td><td> No </td></tr>
                                                                                                                                                                                                                      <tr><td> Primary Care Physician/ Family Doctor </td><td> test </td></tr>
                                                                                                                                                                                                                            <tr><td> Expected Admission Date</td><td> 08 / 31 / 2020 </td></tr>
                                                                                                                                                                                                                                 <tr><td> Admitting / Ordering Physician Name</td><td> test </td></tr>
                                                                                                                                                                                                                                        <tr><td> Type Of Procedure/ Test </td><td> Diagnostic Testing </td></tr>
                                                                                                                                                                                                                                                    <tr>< th colspan = '2' > Responsible Party </ th ></tr><tr><td> Guarantor same as patient</td><td> Yes </td></tr>
                                                                                                                                                                                                                                                                     <tr><td> Guarantor First Name</td><td> test </td></tr>
                                                                                                                                                                                                                                                                          <tr><td> Guarantor Last Name</td><td> test </td></tr>
                                                                                                                                                                                                                                                                               <tr><td> Relationship </td><td> Self </td></tr>
                                                                                                                                                                                                                                                                               <tr><td> Guarantor SSN </td><td> 999 - 99 - 9999 </td></tr>
                                                                                                                                                                                                                                                                                    <tr><td> Guarantor Address </td><td> test </td></tr>
                                                                                                                                                                                                                                                                                         <tr><td> City </td><td> test </td></tr>
                                                                                                                                                                                                                                                                                         <tr><td> Outside of U.S.</td><td> No </td></tr>
                                                                                                                                                                                                                                                                                              <tr><td> State </td><td> Alabama </td></tr>
                                                                                                                                                                                                                                                                                              <tr><td> Country </td><td> United States </td></tr>
                                                                                                                                                                                                                                                                                                         <tr><td> Zip Code </td><td> 85001 </td></tr>
                                                                                                                                                                                                                                                                                                              <tr><td> Telephone Number </td><td> 999 - 999 - 9999 </td></tr>
                                                                                                                                                                                                                                                                                                                   <tr><td> Employment Status </td><td> Active Military Duty</td></tr>
                                                                                                                                                                                                                                                                                                                              <tr><td> Employer Name </td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                   <tr>< th colspan = '2' > Emergency Contact </ th ></tr><tr><td> Emergency Contact same as Guarantor</td><td> No </td></tr>
                                                                                                                                                                                                                                                                                                                                                    <tr><td> Contact Name </td><td> test test </td></tr>
                                                                                                                                                                                                                                                                                                                                                               <tr><td> Relationship </td><td> Grandparent </td></tr>
                                                                                                                                                                                                                                                                                                                                                               <tr><td> Telephone Number </td><td> 999 - 999 - 9999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                    <tr><td> No secondary contact</td><td> Yes </td></tr>
                                                                                                                                                                                                                                                                                                                                                                         <tr><td> Secondary Contact Name</td><td> Not Applicable </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                   <tr><td> Relationship </td><td> Not Applicable </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                              <tr><td> Telephone Number </td><td> Not Applicable </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                         <tr>< th colspan = '2' > Insurance </ th ></tr><tr><td> Are you insured?</td><td> Yes </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                          <tr><td> Primary Insurance Company</td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                               <tr><td> Insurance Company Telephone Number </td><td> 999 - 999 - 9999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <tr><td> Pre - Certification Telephone Number</td><td> 999 - 999 - 9999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                           <tr><td> Insurance Company Address</td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                <tr><td> City </td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                <tr><td> State </td><td> Alabama </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                <tr><td> Zip Code </td><td> 85001 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                     <tr><td> Subscriber same as patient</td><td> No </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                          <tr><td> Subscriber First Name</td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                               <tr><td> Subscriber Last Name</td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <tr><td> Subscriber DOB </td><td> 08 / 02 / 2020 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                         <tr><td> Subscriber SSN </td><td> 999 - 99 - 9999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                              <tr><td> Insurance Policy Number</td><td> 99999999999999999999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <tr><td> Insurance Policy Group Number </td><td> 999999999999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <tr><td> Do you have secondary insurance?</td><td> Yes </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             <tr><td> Secondary Insurance Company Name </td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  <tr><td> Insurance Company Telephone Number </td><td> 999 - 999 - 9999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       <tr><td> Pre - Certification Telephone Number</td><td> 999 - 999 - 9999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              <tr><td> Insurance Company Address</td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <tr><td> City </td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <tr><td> State </td><td> Alabama </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   <tr><td> Zip Code </td><td> 85001 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <tr><td> Subscriber same as patient</td><td> No </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             <tr><td> Subscriber First Name</td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  <tr><td> Subscriber Last Name</td><td> test </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       <tr><td> Subscriber DOB </td><td> 08 / 02 / 2020 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <tr><td> Subscriber SSN </td><td></td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 <tr><td> Insurance Policy Number</td><td> 999999999999999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      <tr><td> Insurance Policy Group Number </td><td> 9999999999999 </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <tr><td> If there is a financial liability(i.e.co - payment, deductible, etc.) what is your preferred method of payment ?</td><td> Credit Card </td></tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 </table>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ";

        }
    }
}
